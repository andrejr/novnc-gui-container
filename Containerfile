# 9000 - nginx

FROM debian:bookworm AS locale

# locales
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get install --no-install-recommends -y locales && \
    sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen && \
    locale-gen && \
    rm -rf /var/lib/apt/lists/*
ENV LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

FROM locale

LABEL maintainer="Andrej Radović (r.andrej@gmail.com)"

ENV XDG_RUNTIME_DIR=/data

RUN apt-get update -y && \
    apt-get install --no-install-recommends -y \
    ca-certificates \
    net-tools \
    nginx \
    novnc \
    openbox \
    procps \
    sudo \
    supervisor \
    tigervnc-standalone-server \
    xorg \
    && \
    ln -s /init /init.entrypoint && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


# copy files
COPY ./docker-root /


RUN sed -i -r \
    -e "s/'resize', 'off'/'resize', 'remote'/g" \
    -e "s/autoconnect = Web.*;/autoconnect = true;/g" \
    /usr/share/novnc/app/ui.js

EXPOSE 9000

ENTRYPOINT ["/app/init.sh"]
