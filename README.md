# novnc-gui-container

[![Quay](https://quay.io/repository/andrej/novnc-gui-container/status "Quay")](https://quay.io/repository/andrej/novnc-gui-container)

A simple image to wrap a GUI application in a container, allowing you to access
its GUI via the browser.
Novnc's web server (proxied through nginx) is on port 9000.
It uses `tigervnc` for the VNC server, `novnc` for the VNC-in-browser
functionality, `nginx` for http proxying, and `supervisord` for managing
multiple processes in the container.


The contained program window is resized with the browser window, which makes
for a native-like experience.

The program (and everything else) inside the container runs as the root user.
This was a conscious choice: I've built the container to be run *rootlessly* in
`podman`.
Podman maps the in-container root user/group's UID and GID to the UID/GID of
the user running the container.
This means that any files in mounted volumes that are created in the container
will have the same permissions as the user running the container.

## Usage

You make a `Containerfile` that uses this container as the base, install
whatever additional deps you have, and replace the `docker-root/app/vncmain.sh`
script with your own script running whatever program you want.

Here's an example:

**Containerfile**:

```Dockerfile
FROM quay.io/andrej/novnc-gui-container:latest

COPY vncmain.sh /app/vncmain.sh

ENV APP_NAME="App name to be used as page title in browser"

RUN apt-get update -y && \
    apt-get install --no-install-recommends -y xterm && \
    rm -rf /var/lib/apt/lists/*
```

**vncmain.sh**:

```bash
#!/bin/bash
xterm
```

Then you may build and run the container with:

```bash
podman build  -t your-container .
podman run --rm -it -p 9000:9000 your-container
```

Bear in mind that the `$HOME` folder is `/data`, so you can mount it in a
volume if you need it (or, say, `$HOME/.config`).
