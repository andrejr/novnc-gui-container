#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -eo pipefail

[ -f /tmp/.X11-lock ] && rm /tmp/.X11-lock

sed -r -i "s#<title>.*</title>#<title>$APP_NAME</title>#g" /usr/share/novnc/vnc.html

/usr/bin/supervisord -c /etc/supervisord.conf
